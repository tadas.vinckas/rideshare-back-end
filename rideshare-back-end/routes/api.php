<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix'=>'auth'], function() {

Route::post('login','MainController@login');
ROute::post('register','MainController@register');

Route::group(['middleware'=>'auth:api'], function() {
    Route::get('logout','MainController@logout');
    Route::get('profile','MainController@profile');
    Route::post('trip','TripsController@createTrip');
    Route::get('trip','TripsController@getTrip');
    Route::post('trip/delete','TripsController@deleteTrip');
    Route::get('all-user-created-trips','TripsController@getAllUserCreatedTrips');
    Route::get('all-trips','TripsController@allTrips');
    Route::post('reservation','ReservationsController@createReservation');
    Route::get('trip/reservations','ReservationsController@tripReservations');
    Route::get('user/reservations','ReservationsController@userReservations');
    Route::post('trip/reservation/passenger/delete','ReservationsController@deletePassenger');
    Route::get('trip/reservation/passenger/delete/reasons','ReservationsController@passengerDeleteReasons');
    Route::post('trip/reservation/delete','ReservationsController@deleteReservation');
    Route::post('trip/reservation/confirm','ReservationsController@confirmReservation');
    Route::post('trip/reservation/passenger/delete','ReservationsController@deletePassenger');
    Route::post('trip/record','TripRecordsController@createTripRecord');
    Route::get('trip/record','TripRecordsController@getTripRecord'); 
    Route::post('create/car','CarController@createCar'); 
    Route::post('edit/car','CarController@editCar'); 
    Route::post('delete/car','CarController@deleteCar'); 
    Route::get('car','CarController@getCar'); 
    Route::get('user/cars','CarController@getAllUserCars'); 
    Route::post('generate-report-driver','ReportController@generateReportDriver'); 
    Route::post('generate-report-traveler','ReportController@generateReportTraveler'); 

});
});