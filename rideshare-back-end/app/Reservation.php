<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Reservation extends Model
{
    public function user() {
        $this->belongsTo(User::class);
    }

    public function trip() {
        $this->belongsTo(Trip::class);
    }
}
