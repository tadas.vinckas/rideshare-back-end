<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car extends Model
{
    public function user() {
        $this->belongsTo(User::class);
    }
}
