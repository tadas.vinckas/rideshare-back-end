<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Car;

class CarController extends Controller
{
    public function createCar(Request $request) { 
        $request->validate([
            'registration_number'=>'required|numeric',
            'producer'=>'required|string',
            'model'=>'required|string',
            'seatsCount'=>'required|numeric',
            'technical_inspection'=>'required|date',
            'insurance'=>'required|date',
            'manufacture'=>'required|date',
            'user_id'=>'required|numeric'
        ]);

        $car = new Car;
        $car->registration_number = $request->registration_number;
        $car->producer = $request->producer;
        $car->model = $request->model;        
        $car->seatsCount = $request->seatsCount;       
        $car->technical_inspection = $request->technical_inspection;
        $car->insuranse = $request->insurance;
        $car->manufacture = $request->manufacture;
        $car->user_id = $request->user_id;

        $car->save();

        return response()->json(["message" => "car was created"]);
    }

    public function editCar(Request $request) { 
        $request->validate([
            'car_id'=>'required|numeric',
            'registration_number'=>'required|numeric',
            'producer'=>'required|string',
            'model'=>'required|string',
            'seatsCount'=>'required|numeric',
            'technical_inspection'=>'required|date',
            'insurance'=>'required|date',
            'manufacture'=>'required|date',
            'user_id'=>'required|numeric'
        ]);

        $car = Car::where('id',$request->car_id)->first();
        $car->registration_number = $request->registration_number;
        $car->producer = $request->producer;
        $car->model = $request->model;        
        $car->seatsCount = $request->seatsCount;       
        $car->technical_inspection = $request->technical_inspection;
        $car->insuranse = $request->insurance;
        $car->manufacture = $request->manufacture;
        $car->user_id = $request->user_id;

        $car->save();

        return response()->json(["message" => "car was updated"]);
    }

    public function deleteCar(Request $request) { 
        $request->validate([
            'car_id'=>'required|numeric',
        ]);

        Car::find($request->car_id)->delete();

        return response()->json(["message" => "car was deleted"]);
    }

    public function getCar(Request $request) { 
        $request->validate([
            'car_id'=>'required|numeric',
        ]);

        $car = Car::find($request->car_id);

        return response()->json(["car information" => $car]);
    }

    public function getAllUserCars(Request $request) { 
        $request->validate([
            'user_id'=>'required|numeric',
        ]);

        $cars = Car::find($request->user_id);

        return response()->json(["cars information" => $cars]);
    }
}
