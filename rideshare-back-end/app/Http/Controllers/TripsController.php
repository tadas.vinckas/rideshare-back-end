<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Trip;
use App\TripRecord;
use App\Reservation;
use Carbon;
use Auth;

class TripsController extends Controller
{
    public function createTrip(Request $request) {

    $request->validate([
        'departure_location'=>'required|string',
        'planned_departure_date'=>'required|date',
        'price'=>'required|numeric',
        'destination_location'=>'required|string',
    ]);

    $mytime = Carbon\Carbon::now();

    $trip = new Trip;
    $trip->departure_location = $request->departure_location;
    $trip->planned_departure_date = Carbon\Carbon::parse($request->planned_departure_date);
    $trip->planned_distance = $request->planned_distance;
    $trip->price = $request->price;
    $trip->user_id = Auth::user()->id;
    $trip->destination_location = $request->destination_location;
    $trip->date = $mytime->toDateTimeString();
    $trip->save();
    
    return response()->json(["message" => "trip was created"]);
    }

    public function getTrip(Request $request) {
        $request->validate([
            'trip_id'=>'required|numeric'
        ]);

        $trip = new Trip;
        $trip = Trip::where('id',$request->trip_id)->first();
        
        return response()->json(["trip" => $trip]);
    }
    
    public function getAllUserCreatedTrips(Request $request) {

        $trips = Trip::where('user_id',Auth::user()->id)->get();
        
        return response()->json(["trips" => $trips]);
    }

    public function allTrips(Request $request) {

        $trips = Trip::all();
        
        return response()->json(["trips" => $trips]);
    } 
    
    public function deleteTrip(Request $request) {

        $request->validate([
            'trip_id'=>'required|numeric'
        ]);
       
        Reservation::where('trip_id',$request->trip_id)->delete();
        TripRecord::where('trip_id',$request->trip_id)->delete();
        Trip::where('id',$request->trip_id)->delete();
        
        return response()->json(["message" => "trip was deleted"]);
    } 

}
