<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\TripRecord;
use App\User;
use App\Reservation;
use App\Trip;

class TripRecordsController extends Controller
{
    public function createTripRecord(Request $request) {
        $request->validate([
            'trip_end_date'=>'required|date',
            'fuel_cost'=>'required|numeric',
            'distance'=>'required|numeric',
            'trip_id'=>'required|numeric'
        ]);
    
        $trip_record = new TripRecord;
        $trip_record->trip_end_date = $request->trip_end_date;
        $trip_record->fuel_cost = $request->fuel_cost;
        $trip_record->distance = $request->distance;
        $trip_record->trip_id = $request->trip_id;
        $trip_record->save();
        
        return response()->json(["message" => "trip record was created"]);
    }

    public function getTripRecord(Request $request) {
        $request->validate([
            'trip_id'=>'required|numeric'
        ]);
    
        $trip_record = TripRecord::where('trip_id',$request->trip_id)->first();

        return response()->json(["trip record" => $trip_record]);
    }
}
