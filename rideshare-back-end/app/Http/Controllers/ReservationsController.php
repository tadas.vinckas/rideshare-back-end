<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Reservation;
use App\Trip;
use App\DeletedPassenger;
use Auth;
use Carbon;

class ReservationsController extends Controller
{
    public function createReservation(Request $request) {

        $request->validate([
            'placesCount'=>'required|integer',
            'trip_id'=>'required|numeric'
        ]);

        $mytime = Carbon\Carbon::now();
        
        $reservation = new Reservation;
        $reservation->date = $mytime->toDateTimeString();
        $reservation->isConfirmed = 0;
        $reservation->placesCount = $request->placesCount;
        $reservation->trip_id = $request->trip_id;
        $reservation->user_id = Auth::user()->id;
        $reservation->save();

        return response()->json(["message" => "reservation was created"]);
    }

    public function tripReservations(Request $request) {

        $request->validate([
            'trip_id'=>'required|numeric'
        ]);

        $reservations = Reservation::where('trip_id',$request->trip_id)->get();

        foreach($reservations as $reservation) { 
            $trip = Trip::where('id',$reservation->trip_id)->first();
            $reservation->destination_location = $trip->destination_location;
        }

        return response()->json(["reservations" => $reservations]);
    }

    public function userReservations(Request $request) {


        $reservations = Reservation::where('user_id',Auth::user()->id)->get();

        foreach($reservations as $reservation) { 
            $trip = Trip::where('id',$reservation->trip_id)->first();
            $reservation->destination_location = $trip->destination_location;

            $user = User::where('id',$trip->user_id)->first();
            $reservation->user_email = $user->email;
        }
        return response()->json(["reservations" => $reservations]);
    }

    public function confirmReservation(Request $request) {

        $request->validate([
            'reservation_id'=>'required|numeric'
        ]);

        $reservation = Reservation::where('id',$request->reservation_id)->first();
        $reservation->isConfirmed = 1;
        $reservation->save();

        return response()->json(["confirm was created"]);
    }

    public function deleteReservation(Request $request) {

        $request->validate([
            'reservation_id'=>'required|numeric'
        ]);

        Reservation::where('id',$request->reservation_id)->delete();

        return response()->json(["reservation was deleted"]);
    }

    public function deletePassenger(Request $request) {

        $request->validate([
            'reservation_id'=>'required|numeric',
            'reason'=>'required|string',
            'date'=>'required|date'
        ]);

        $deletedPassenger = new DeletedPassenger;
        $reservation = Reservation::where('id',$request->reservation_id)->first();
        $deletedPassenger->user_id = $reservation->user_id;
        $deletedPassenger->trip_id = $reservation->trip_id;
        $reservation->delete();

        $deletedPassenger->date = $request->date;
        $deletedPassenger->reason = $request->reason;
        $deletedPassenger->save();

        return response()->json(["passenger reservation was deleted and reason created"]);
    }

    public function passengerDeleteReasons(Request $request) { 
        $request->validate([
            'user_id'=>'required|numeric'
        ]);

        $deletedPassenger = DeletedPassenger::where('user_id',$request->user_id)->get();

        return response()->json(["passenger reservations delete reasons" => $deletedPassenger]);
    }
}
