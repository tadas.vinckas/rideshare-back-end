<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Carbon\Carbon;
use Auth;
use App\Trip;

class MainController extends Controller
{
    public function createTrip(Request $request) {

        $request->validate([
            'date'=>'required',
            'departure_location'=>'required',
            'plannned_departure_date'=>'required',
            'planned_distance'=>'required',
            'price'=>'required',
            'user_id'=>'required'
        ]);
        
    
        $trip = Trip::create(['date' => $request->date],
        ['departure_location' => $request->departure_location],
        ['planned_departure_date' => $request->planned_departure_date],
        ['planned_distance' => $request->planned_distance],
        ['user_id'=> $request->user_id],
        ['price'=>$request->price]
        );
        
        return response()->json(["message" => "trip was created"]);
    }

    public function register(Request $request) { 
    $request->validate([
        'name' => 'required|string',
        'email' => 'required|string|email|unique:users',
        'password' => 'required|string|confirmed',
        'role' => 'required'
    ]);

    $user = new User([
        'name' => $request->name,
        'email'=>$request->email,
        'password'=>bcrypt($request->password),
        'role'=> $request->role
    ]);

    $user->save();

    return response()->json([
        'message'=>'User was created'
    ], 201);
    }

    public function login(Request $request) { 
        $request->validate([
            'email'=>'required|string|email',
            'remember_me'=>'boolean'
        ]);

        $userCredentials = request(['email','password']);

        if(!Auth::attempt($userCredentials)) { 
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);
        }

        $user = $request->user();

        $tokenResult = $user->createToken('User Personal Access Token');

        $token = $tokenResult->token;

        if($request->remember_me) { 
            $token->expires_at = Carbon::now()->addWeeks(3);
        }

        $token->save();
    
        return response()->json([
            'access_token'=>$tokenResult->accessToken,
            'token_type'=>'Bearer',
            'role' => Auth::user()->role,
            'expired_at'=>Carbon::parse($tokenResult->token->expires_at)->toDateString()
        ]);
    }

    public function profile(Request $request) { 
        return response()->json($request->user());
    }
}
