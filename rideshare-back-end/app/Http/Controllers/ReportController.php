<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Trip;
use Auth;
use Carbon;
use App\Reservation;

class ReportController extends Controller
{
    /*
    public function generateReportDriver() { 
       
    //    $from = Carbon::parse($request->from);
    //    $to = Carbon::parse($request->to);

        $drivers = User::where('role','driver')->get();
        
        $temp = array(
            "distance" => "",
            "distance1" => "");

        $driver_results = array();     

        foreach ($drivers as $driver){ 
        $temp["distance"] = Trip::where('user_id',$driver->id)->sum('planned_distance');
        $temp["distance1"] = Trip::where('user_id',$driver->id)->sum('planned_distance');
        array_push($driver_results,$temp);
        }

        return response()->json(["" => $driver_results]);
      //  Trip::whereBetween('date', [$from, $to])->get();
    }
    */

    public function generateReportDriver(Request $request) { 
       
           $from = Carbon\Carbon::parse($request->from);
           $to = Carbon\Carbon::parse($request->to);
    
            $driver = User::find(Auth::user()->id);
    
            $travels_count =  Trip::where('user_id',$driver->id)->whereBetween('date', [$from, $to])->count();
            $distance_travelled = Trip::where('user_id',$driver->id)->whereBetween('date', [$from, $to])->sum('planned_distance');
            $income = Trip::where('user_id',$driver->id)->whereBetween('date', [$from, $to])->sum('price');
            return response()->json(array("trips_count"=>$travels_count,"distance_sum" => $distance_travelled, "income" => $income));

    }

    
    public function generateReportTraveler(Request $request) { 
       
         $from = Carbon\Carbon::parse($request->from);
         $to = Carbon\Carbon::parse($request->to);
 
         $traveler = User::find(Auth::user()->id);

// todo date
 
        $reservations_count =  Reservation::where('user_id',$traveler->id)->whereBetween('date', [$from, $to])->count();
      $traveler_reservations_id =  Reservation::where('user_id',$traveler->id)->pluck('trip_id');
      $distance_travelled = Trip::whereIn('id',$traveler_reservations_id)->sum('planned_distance');
     $expenses = Trip::whereIn('id',$traveler_reservations_id)->sum('price');

        //      $distance_travelled = Trip::where('user_id',$driver->id)->whereBetween('date', [$from, $to])->sum('planned_distance');
 //        $expenses = Trip::where('user_id',$driver->id)->whereBetween('date', [$from, $to])->sum('price');
 //return response()->json(["reservations_count"=>$reservations_count]);
 return response()->json(array("reservations_count"=>$reservations_count,"distance_sum" => $distance_travelled, "expenses" => $expenses));

 }
}
