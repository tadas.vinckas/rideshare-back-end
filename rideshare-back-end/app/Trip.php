<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
     protected $fillable = [
    'date', 'departure_location', 'planned_departure_date','price','user_id', 'planned_distance'
    ];

    public function user() {
        $this->belongsTo(User::class);
    }
    
    public function car() {
        $this->belongsTo(Car::class);
    }
    
    public function reservation()
    {
        return $this->hasMany(Reservation::class);
    }

    public function deletedPassenger()
    {
        return $this->hasMany(DeletedPassenger::class);
    }

    public function reservationRecord()
    {
        return $this->hasOne(TripRecord::class);
    }
}
