<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class TripRecord extends Model
{
    public function trip() {
        $this->belongsTo(Trip::class);
    }
}
