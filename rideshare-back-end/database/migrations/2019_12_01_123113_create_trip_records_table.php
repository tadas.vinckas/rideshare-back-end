<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTripRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip_records', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('trip_end_date');
            $table->decimal('fuel_cost', 8, 2);
            $table->decimal('distance',8,2);
            $table->bigInteger('trip_id')->unsigned()->index()->nullable();
            $table->foreign('trip_id')->references('id')->on('trips');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip_records');
    }
}
