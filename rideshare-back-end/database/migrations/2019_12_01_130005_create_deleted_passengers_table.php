<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeletedPassengersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('deleted_passengers', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->date('date');
            $table->string('reason');
            $table->bigInteger('trip_id')->unsigned()->index()->nullable();
            $table->foreign('trip_id')->references('id')->on('trips');
            $table->bigInteger('user_id')->unsigned()->index()->nullable();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('deleted_passengers');
    }
}
